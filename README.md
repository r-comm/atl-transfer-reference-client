Source content: https://github.com/atlassian-labs/transfer-api-ref-client

Get started by setting up container:
```
docker run --rm -it --name transfer-reference-client \
    --volume <HOST_PATH>:/upload \
    roncyj/atl-transfer-reference-client python3 app.py \
    --file /upload/<FileName> \                        
     --issue_key=<ISSUE_KEY> \
     --user=<API_USER_ID> \
     --auth_token=<API_TOKEN>
```
